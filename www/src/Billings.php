<?php


namespace App;

use PDO;
use App\Database;

class Billings extends Database
{
    public $id, $income, $description, $time;


    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray["id"];

        if(array_key_exists("type",$postArray))
            $this->type = $postArray["type"];

        if(array_key_exists("income",$postArray))
            $this->income = $postArray["income"];


        if(array_key_exists("description",$postArray))
            $this->description = $postArray["description"];

        $this->time= date('d-m-y');

    }


    public function store(){

        $sqlQuery = "INSERT INTO price (type, time, amount, description)  VALUES ( ?, ?, ?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [$this->type , $this->time, $this->income, $this->description];

        $sth->execute($dataArray);

    }

    public function indexIncome(){
        $sqlQuery = "SELECT * FROM price WHERE type='income' ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;


    }// end of index() Method

    public function indexExpense(){
        $sqlQuery = "SELECT * FROM price WHERE type='expense' ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;


    }

    public function totalIncome(){
        $today = date('d-m-y');
        $sqlQuery = "SELECT SUM(amount) as value FROM price WHERE type='income' AND time='{$today}'";
        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;
    }

    public function totalExpense(){
        $todays = date('d-m-y');
        $sqlQuery = "SELECT SUM(amount) as value FROM price WHERE type='expense' AND time='{$todays}'";
        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;
    }

    public function delete(){

        $sqlQuery = "DELETE FROM price where id=".$this->id;

        $this->dbh->exec($sqlQuery);



    }
}