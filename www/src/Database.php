<?php
/**
 * Created by PhpStorm.
 * User: FAHIM HASAN
 * Date: 4/26/2018
 * Time: 1:20 AM
 */

namespace App;

use PDO,PDOException;

class Database
{
protected $dbh;

public function __construct()
{

    try{

        $this->dbh = new PDO("mysql:host=localhost;dbname=techtree_software", "root", "");

        $this->dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );


    }
    catch (PDOException $error){
        echo "OPPS! Something went wrong";
    }
}
}