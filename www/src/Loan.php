<?php
/**
 * Created by PhpStorm.
 * User: FAHIM HASAN
 * Date: 4/26/2018
 * Time: 1:19 AM
 */

namespace App;

use PDO;
use App\Database;

class Loan extends Database
{
    public $id, $income, $description, $time;


    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray["id"];

        if(array_key_exists("type",$postArray))
            $this->type = $postArray["type"];

        if(array_key_exists("income",$postArray))
            $this->income = $postArray["income"];


        if(array_key_exists("description",$postArray))
            $this->description = $postArray["description"];

        $this->time= date('d-m-y');

    }


    public function store(){

        $sqlQuery = "INSERT INTO loan (type, time, amount, description)  VALUES ( ?, ?, ?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [$this->type , $this->time, $this->income, $this->description];

        $sth->execute($dataArray);

    }

    public function indexIncome(){
        $sqlQuery = "SELECT * FROM loan WHERE type='income' ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;


    }// end of index() Method

    public function indexExpense(){
        $sqlQuery = "SELECT * FROM loan WHERE type='expense' ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;


    }

    public function totalIncome(){
        $sqlQuery = "SELECT SUM(amount) as value FROM loan WHERE type='income'";
        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;
    }

    public function totalExpense(){
        $sqlQuery = "SELECT SUM(amount) as value FROM loan WHERE type='expense'";
        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;
    }

    public function delete(){

        $sqlQuery = "DELETE FROM loan where id=".$this->id;

        $this->dbh->exec($sqlQuery);



    }
}