<?php
require_once("../vendor/autoload.php");
$obj  = new \App\Billings();
$obj->setData($_POST);
$obj->store();
return header("Location: {$_SERVER['HTTP_REFERER']}");
