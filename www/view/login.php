<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="../resources/css/stylelogin.css">
    <title>Techtree | Cooperation is the best help</title>
</head>
<body class="text-center">
<form class="form-signin">
    <img class="mb-4" src="../resources/images/logo-color.png" alt="">

    <div class="form-group">
    <label for="inputEmail" class="sr-only">User Name</label>
    <input type="text" id="inputEmail" class="form-control" placeholder="User Name" required autofocus>
    </div>

    <div class="form-group">
    <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Log
    In</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
</form>
<?php include('footer.php'); ?>