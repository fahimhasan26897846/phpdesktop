<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><img src="../resources/images/logo-color.png" alt="logo" height="56px" width="200"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if ($active == "expenses")echo "active"; ?>  pl-5">
                <a class="nav-link" href="dashboard.php">HOME</a>
            </li>
            <li class="nav-item <?php if ($active == "loan")echo "active"; ?> pl-5">
                <a class="nav-link " href="loan.php">LOAN</a>
            </li>
            <li class="nav-item <?php if ($active == "accounts")echo "active"; ?> pl-5">
                <a class="nav-link " href="accounts.php">ACCOUNTS</a>
            </li>
            <li class="nav-item pl-5">
                <a class="nav-link " href="#">LOGOUT</a>
            </li>
        </ul>
    </div>
</nav>