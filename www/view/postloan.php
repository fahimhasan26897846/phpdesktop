<?php
require_once("../vendor/autoload.php");
$obj  = new \App\Loan();
$obj->setData($_POST);
$obj->store();
return header("Location: {$_SERVER['HTTP_REFERER']}");
