<?php include('header.php');
include_once ('../vendor/autoload.php');
$income = new \App\Loan();
$incomes = $income->indexIncome();
$expense = $income->indexExpense();
$today = $income->totalIncome();
$todayEx = $income->totalExpense();
$ex = $todayEx[0];
$thatDay = $today[0];
$active = "loan";
$row = 1;
$rows = 1;
?>
<body>
<?php include('nav.php');?>
<section>
    <div class="container">
        <div class="row m-1 p-1 bg-white">
            <button class="btn btn-sm btn-primary m-1" data-toggle="modal" data-target="#add-income"><i class="fas fa-plus-circle"></i> ADD Loan</button>
            <!-- Modal -->
            <div class="modal fade" id="add-income" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="postloan.php" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="type" value="income">
                                    <input type="number" name="income" class="form-control" placeholder="Amount">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="description" class="form-control" placeholder="DESCRIPTION">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save"></i>&nbsp;ADD</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <button class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#remove"><i class="fas fa-minus-circle"></i>Withdrow Loan</button>
            <div class="modal fade" id="remove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="postloan.php" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="type" value="expense">
                                    <input type="number" name="income" class="form-control" placeholder="Amount">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="description" class="form-control" placeholder="DESCRIPTION">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-save"></i>&nbsp;ADD</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <button class="btn btn-warning  mr-2 btn-sm"> total Income: <?php print_r($thatDay->value) ?></button>
            <button class="btn btn-secondary  mr-2 btn-sm">total Expense: <?php print_r($ex->value) ?></button>
            <button class="btn btn-primary  mr-2  btn-sm">total: <?php $sum = $thatDay->value - $ex->value; echo $sum; ?></button>
        </div>
    </div>

</section>

<sectiion>
    <div class="container">
        <h4>Loan List</h4>
        <table class="table table-responsive-sm">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Number</th>
                <th scope="col">DATE</th>
                <th scope="col">AMOUNT</th>
                <th scope="col">DESCRIPTION</th>
                <th scope="col">ACTIONS</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($incomes as $single) {
                echo "<tr>
                      <th scope='row'>$row</th>
                      <td>$single->time</td>
                      <td>$single->amount</td>
                      <td>$single->description</td>
                      <td><a
                            href='deleteloan.php?id=$single->id' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Trash\"><i class=\"fas fa-trash-alt text-danger pr-1\"></i></a></td>
                      </tr>
";
                $row ++; } ?>

            </tbody>
        </table>
        <h4>Withdrow List</h4>
        <table class="table table-responsive-sm">
            <thead class="thead-light">
            <tr>
                <th scope="col">Number</th>
                <th scope="col">DATE</th>
                <th scope="col">AMOUNT</th>
                <th scope="col">DESCRIPTION</th>
                <th scope="col">ACTIONS</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($expense as $singles) {
                echo "<tr>
                      <th scope='row'>$rows</th>
                      <td>$singles->time</td>
                      <td>$singles->amount</td>
                      <td>$singles->description</td>
                      <td><a
                            href='deleteloan.php?id=$singles->id' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Trash\"><i class=\"fas fa-trash-alt text-danger pr-1\"></i></a></td>
                      </tr>
";
                $rows ++; } ?>
            </tbody>
        </table>
    </div>

</sectiion>
<section>
    <div id="calculator">
        <!-- Screen and clear key -->
        <div class="top">
            <span class="clear">C</span>
            <div class="screen"></div>
        </div>

        <div class="keys">
            <!-- operators and other keys -->
            <span>7</span>
            <span>8</span>
            <span>9</span>
            <span class="operator">+</span>
            <span>4</span>
            <span>5</span>
            <span>6</span>
            <span class="operator">-</span>
            <span>1</span>
            <span>2</span>
            <span>3</span>
            <span class="operator">÷</span>
            <span>0</span>
            <span>.</span>
            <span class="eval">=</span>
            <span class="operator">x</span>
        </div>
    </div>
</section>
</body>
<?php include('footer.php'); ?>
